
CREATE TABLE `feature_toggle` (
    `id` BIGINT(20) AUTO_INCREMENT PRIMARY KEY,
    `display_name` VARCHAR(100),
    `technical_name` VARCHAR(100) NOT NULL UNIQUE,
    `expires_on` TIMESTAMP NULL,
    `description` VARCHAR(500),
    `inverted` TINYINT(1) NOT NULL DEFAULT 0,
    `archived` TINYINT(1) NOT NULL DEFAULT 0
);

CREATE TABLE `customer` (
    `id` BIGINT(20) AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(100) NOT NULL UNIQUE
);

CREATE TABLE `feature_toggle_customer` (
    `feature_toggle_id` BIGINT(20) NOT NULL,
    `customer_id` BIGINT(20) NOT NULL,
    PRIMARY KEY (`feature_toggle_id`, `customer_id`),
    CONSTRAINT `FK_feature_toggle_customer_param_feature_toggle_id` FOREIGN KEY (`feature_toggle_id`)
        REFERENCES `feature_toggle` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT `FK_feature_toggle_customer_param_customer_id` FOREIGN KEY (`customer_id`)
       REFERENCES `customer` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
);

/** Create default customers */

INSERT INTO `customer` (`name`)
VALUES ('Customer 1'),
       ('Customer 2'),
       ('Customer 3'),
       ('Customer 4'),
       ('Customer 5'),
       ('Customer 6'),
       ('Customer 7'),
       ('Customer 8'),
       ('Customer 9'),
       ('Customer 10');