import { Component, OnInit } from '@angular/core';
import {FeatureToggleService} from '../service/feature-toggle.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {FeatureToggleModel} from '../model/feature-toggle-model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-feature-toggle-create',
  templateUrl: './feature-toggle-create.component.html',
  styleUrls: ['./feature-toggle-create.component.scss']
})
export class FeatureToggleCreateComponent implements OnInit {

  featureToggleForm = new FormGroup({
    technicalName: new FormControl('',[Validators.required]),
    displayName: new FormControl(''),
    description: new FormControl(''),
    expiresOn: new FormControl(''),
    inverted: new FormControl(false),
    customers: new FormControl([], [Validators.required])
  });

  constructor(private featureToggleService: FeatureToggleService, private router: Router) { }

  ngOnInit(): void {

  }

  submit() {

    const newToggle = new FeatureToggleModel();

    newToggle.technicalName = this.featureToggleForm.value.technicalName;
    newToggle.displayName = this.featureToggleForm.value.displayName;
    newToggle.description = this.featureToggleForm.value.description;
    newToggle.expiresOn = this.featureToggleForm.value.expiresOn;
    newToggle.inverted = this.featureToggleForm.value.inverted;
    newToggle.customerIds = this.featureToggleForm.value.customers;

    this.featureToggleService.createFeatureToggle(newToggle).subscribe((toggle: FeatureToggleModel) => {
      this.router.navigate(['']).then();
    });
  }

  cancel() {
    this.router.navigate(['']).then();
  }
}
