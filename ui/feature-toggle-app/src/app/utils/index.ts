import {HttpHeaders} from '@angular/common/http';

const headers = new HttpHeaders({
  'Content-Type': 'application/json',
  'Cache-Control': 'no-cache'
});

export const defaultHttpOptions = {
  headers
};
