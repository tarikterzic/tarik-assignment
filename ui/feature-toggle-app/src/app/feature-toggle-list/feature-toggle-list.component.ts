import { Component, OnInit } from '@angular/core';
import {FeatureToggleService} from '../service/feature-toggle.service';
import {FeatureToggleModel} from '../model/feature-toggle-model';
import {Router} from '@angular/router';
import {CustomerService} from '../service/customer.service';
import {Customer} from '../model/customer';

@Component({
  selector: 'app-feature-toggle-list',
  templateUrl: './feature-toggle-list.component.html',
  styleUrls: ['./feature-toggle-list.component.scss']
})
export class FeatureToggleListComponent implements OnInit {

  featureToggles: FeatureToggleModel[] = [];
  customers: Customer[] = [];

  constructor(private featureToggleService: FeatureToggleService, public router: Router
              , private customerService: CustomerService) {

  }

  ngOnInit(): void {
    this.getFeatureToggles();
  }

  getFeatureToggles() {
    this.featureToggleService.getFeatureToggles().pipe().subscribe((toggles: FeatureToggleModel[]) => {
        this.featureToggles = toggles;
    });
  }

  edit(toggle: FeatureToggleModel) {
    this.router.navigate(['/edit', toggle.technicalName]).then();
  }

  archive(toggle: FeatureToggleModel) {
    this.featureToggleService.archiveFeatureToggle(toggle).pipe().subscribe((toggle: FeatureToggleModel) => {
      // remove archived item
      this.featureToggles = this.featureToggles.filter(item => item.technicalName !== toggle.technicalName);
    });
  }


}
