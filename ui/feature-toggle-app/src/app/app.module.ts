import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FeatureToggleListComponent } from './feature-toggle-list/feature-toggle-list.component';
import {HttpClientModule} from '@angular/common/http';
import { FeatureToggleCreateComponent } from './feature-toggle-create/feature-toggle-create.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { FeatureToggleEditComponent } from './feature-toggle-edit/feature-toggle-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    FeatureToggleListComponent,
    FeatureToggleCreateComponent,
    FeatureToggleEditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
