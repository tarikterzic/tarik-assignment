import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {FeatureToggleModel} from '../model/feature-toggle-model';
import {defaultHttpOptions} from '../utils';

@Injectable({
  providedIn: 'root'
})
export class FeatureToggleService {

  private FEATURE_TOGGLE_URL = 'http://localhost:8080/featureToggles'

  constructor(private http: HttpClient) { }

  getFeatureToggles(): Observable<FeatureToggleModel[]> {
    return this.http.get<FeatureToggleModel[]>(this.FEATURE_TOGGLE_URL, defaultHttpOptions);
  }

  createFeatureToggle(toggle: FeatureToggleModel): Observable<FeatureToggleModel> {
    return this.http.post<FeatureToggleModel>(this.FEATURE_TOGGLE_URL, toggle, defaultHttpOptions);
  }

  updateFeatureToggle(toggle: FeatureToggleModel): Observable<FeatureToggleModel> {
    return this.http.put<FeatureToggleModel>(this.FEATURE_TOGGLE_URL, toggle, defaultHttpOptions);
  }

  archiveFeatureToggle(toggle: FeatureToggleModel): Observable<FeatureToggleModel> {
    return this.http.put<FeatureToggleModel>(this.FEATURE_TOGGLE_URL + '/' + toggle.technicalName, defaultHttpOptions);
  }
}
