export class FeatureToggleModel {
  displayName: string | undefined;
  technicalName: string | undefined;
  expiresOn: Date | undefined;
  description: string | undefined;
  inverted: boolean | undefined;
  customerIds: string[] | undefined;
}
