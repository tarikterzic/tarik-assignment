import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {FeatureToggleCreateComponent} from './feature-toggle-create/feature-toggle-create.component';
import {FeatureToggleListComponent} from './feature-toggle-list/feature-toggle-list.component';
import {FeatureToggleEditComponent} from './feature-toggle-edit/feature-toggle-edit.component';

const routes: Routes = [
  {
    path: '',
    component: FeatureToggleListComponent
  },
  {
    path: 'create',
    component: FeatureToggleCreateComponent
  },
  {
    path: 'edit/:technicalName',
    component: FeatureToggleEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
