import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {FeatureToggleService} from '../service/feature-toggle.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FeatureToggleModel} from '../model/feature-toggle-model';

@Component({
  selector: 'app-feature-toggle-edit',
  templateUrl: './feature-toggle-edit.component.html',
  styleUrls: ['./feature-toggle-edit.component.scss']
})
export class FeatureToggleEditComponent implements OnInit {

  technicalName: string = '';

  featureToggleForm = new FormGroup({
    technicalName: new FormControl('',[Validators.required]),
    displayName: new FormControl(''),
    description: new FormControl(''),
    expiresOn: new FormControl(''),
    inverted: new FormControl(false),
    customers: new FormControl([], [Validators.required])
  });

  constructor(private featureToggleService: FeatureToggleService, private router: Router, private route: ActivatedRoute,) { }

  ngOnInit(): void {
    this.route.params
      .pipe()
      .subscribe(params => {
        this.technicalName = params['technicalName'];
      });

    this.getFeatureToggleByTechnicalName();
  }

  submit() {

    const updateToggle = new FeatureToggleModel();

    this.featureToggleForm.controls['technicalName'].enable();
    updateToggle.technicalName = this.featureToggleForm.value.technicalName;
    updateToggle.displayName = this.featureToggleForm.value.displayName;
    updateToggle.description = this.featureToggleForm.value.description;
    updateToggle.expiresOn = this.featureToggleForm.value.expiresOn;
    updateToggle.inverted = this.featureToggleForm.value.inverted;
    updateToggle.customerIds = this.featureToggleForm.value.customers;

    this.featureToggleService.updateFeatureToggle(updateToggle).subscribe((toggle: FeatureToggleModel) => {
      this.router.navigate(['']).then();
    });
  }

  cancel() {
    this.router.navigate(['']).then();
  }

  getFeatureToggleByTechnicalName() {
    this.featureToggleService.getFeatureToggles().pipe().subscribe((toggles: FeatureToggleModel[]) => {

      const featureToggleModel: FeatureToggleModel = toggles.find((toggle) => {
        return toggle.technicalName === this.technicalName;
      }) as FeatureToggleModel;

      this.featureToggleForm.controls['technicalName'].setValue(featureToggleModel.technicalName);
      this.featureToggleForm.controls['technicalName'].disable();
      this.featureToggleForm.controls['displayName'].setValue(featureToggleModel.displayName);
      this.featureToggleForm.controls['description'].setValue(featureToggleModel.description);
      this.featureToggleForm.controls['expiresOn'].setValue(featureToggleModel.expiresOn);
      this.featureToggleForm.controls['inverted'].setValue(featureToggleModel.inverted);
      this.featureToggleForm.controls['customers'].setValue(featureToggleModel.customerIds);

    });

  }
}
