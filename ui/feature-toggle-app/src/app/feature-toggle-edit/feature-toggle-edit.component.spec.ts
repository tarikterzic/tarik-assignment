import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FeatureToggleEditComponent } from './feature-toggle-edit.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';

describe('FeatureToggleEditComponent', () => {
  let component: FeatureToggleEditComponent;
  let fixture: ComponentFixture<FeatureToggleEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FeatureToggleEditComponent ],
      imports: [HttpClientTestingModule, RouterTestingModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FeatureToggleEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
