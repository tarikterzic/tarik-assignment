## Prerequisites

In order to run these apps, following software/tools are needed:
- docker with docker compose
- Maven build tool
- JDK 11
- Node and npm

### How to start database container

    docker-compose up

### How to build backend app

    mvn clean package

### How to run backend app

    java -jar feature-toggle-management.jar

or

    mvn spring-boot:run

### How to build fe app
in ui/feature-toggle-app directory run:
    
    npm install

### How to run fe app
in ui/feature-toggle-app directory run:

    ng serve

### How to use app

You can open fe app: http://localhost:4200/
You can open be api docs: http://localhost:8080/swagger-ui.html

### Assumptions

- Authentication and authorization are out of scope
- Customer entities are already predefined and api does not provide endpoints to work with this entity
- UI design is out of scope
