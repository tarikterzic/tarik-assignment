package ch.swisscom.management.controller;

import ch.swisscom.management.dto.FeatureRequestDTO;
import ch.swisscom.management.dto.FeatureResponseDTO;
import ch.swisscom.management.dto.FeatureResponseWrapperDTO;
import ch.swisscom.management.service.IFeatureToggleService;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(FeatureAPIController.class)
public class FeatureAPIControllerTest {

    @MockBean
    private IFeatureToggleService featureToggleService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void compareFeaturesByCustomerOKTest() throws Exception {

        Mockito.when(featureToggleService.compareFeaturesByCustomer(Mockito.any(FeatureRequestDTO.class)))
                .thenReturn(FeatureResponseWrapperDTO.builder()
                        .features(List.of(FeatureResponseDTO.builder()
                        .name("feature1")
                        .active(false)
                        .expired(true)
                        .inverted(true)
                        .build()))
                        .build());

        MvcResult mvcResult = mockMvc.perform(post("/api/v1/features")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"customerId\":\"1\",\"features\":[{\"name\":\"feature1\"},{\"name\":\"feature2\"}]}"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn();

        Assertions.assertEquals("{\"features\":[{\"name\":\"feature1\",\"active\":false,\"inverted\":true,\"expired\":true}]}"
                , mvcResult.getResponse().getContentAsString());

    }


}
