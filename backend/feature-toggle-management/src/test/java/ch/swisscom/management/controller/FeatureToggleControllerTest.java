package ch.swisscom.management.controller;

import ch.swisscom.management.dto.FeatureToggleDTO;
import ch.swisscom.management.service.IFeatureToggleService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(FeatureToggleController.class)
public class FeatureToggleControllerTest {

    @MockBean
    private IFeatureToggleService featureToggleService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void createFeatureToggleOKTest() throws Exception {
        FeatureToggleDTO featureToggleDTO = FeatureToggleDTO.builder()
                .technicalName("feature1")
                .inverted(false)
                .customerIds(List.of("1"))
                .build();

        Mockito.when(featureToggleService.createFeatureToggle(Mockito.any(FeatureToggleDTO.class)))
                .thenReturn(featureToggleDTO);

        mockMvc.perform(post("/featureToggles")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"technicalName\":\"feature1\", \"inverted\":false,\"customerIds\":[\"1\"]}"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.technicalName").value(featureToggleDTO.getTechnicalName()))
                .andExpect(jsonPath("$.inverted").value(featureToggleDTO.getInverted()));

    }

    @Test
    public void updateFeatureToggleOkTest() throws Exception {
        FeatureToggleDTO featureToggleDTO = FeatureToggleDTO.builder()
                .technicalName("feature1")
                .inverted(false)
                .customerIds(List.of("1"))
                .build();

        Mockito.when(featureToggleService.updateFeatureToggle(Mockito.any(FeatureToggleDTO.class)))
                .thenReturn(featureToggleDTO);

        mockMvc.perform(put("/featureToggles")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"technicalName\":\"feature1\", \"inverted\":true,\"customerIds\":[\"1\"]}"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.technicalName").value(featureToggleDTO.getTechnicalName()))
                .andExpect(jsonPath("$.inverted").value(featureToggleDTO.getInverted()));

    }

    @Test
    public void archiveFeatureToggleOkTest() throws Exception {
        FeatureToggleDTO featureToggleDTO = FeatureToggleDTO.builder()
                .technicalName("feature1")
                .inverted(false)
                .customerIds(List.of("1"))
                .build();

        Mockito.when(featureToggleService.archiveFeatureToggle("feature1"))
                .thenReturn(featureToggleDTO);

        mockMvc.perform(put("/featureToggles/feature1")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.technicalName").value(featureToggleDTO.getTechnicalName()))
                .andExpect(jsonPath("$.inverted").value(featureToggleDTO.getInverted()));

    }

    @Test
    public void getAllFeatureToggleOkTest() throws Exception {
        FeatureToggleDTO featureToggleDTO = FeatureToggleDTO.builder()
                .technicalName("feature1")
                .inverted(false)
                .customerIds(List.of("1"))
                .build();

        Mockito.when(featureToggleService.getAllFeatureToggles())
                .thenReturn(List.of(featureToggleDTO));

        mockMvc.perform(get("/featureToggles")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$[0].technicalName").value(featureToggleDTO.getTechnicalName()))
                .andExpect(jsonPath("$[0].inverted").value(featureToggleDTO.getInverted()));

    }
}
