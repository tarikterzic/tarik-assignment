package ch.swisscom.management.service;

import ch.swisscom.management.dto.FeatureDTO;
import ch.swisscom.management.dto.FeatureRequestDTO;
import ch.swisscom.management.dto.FeatureResponseWrapperDTO;
import ch.swisscom.management.dto.FeatureToggleDTO;
import ch.swisscom.management.persistence.entity.Customer;
import ch.swisscom.management.persistence.entity.FeatureToggle;
import ch.swisscom.management.persistence.repository.CustomerRepository;
import ch.swisscom.management.persistence.repository.FeatureToggleRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
public class FeatureToggleServiceImplTest {

    @Mock
    private CustomerRepository customerRepository;

    @Mock
    private FeatureToggleRepository featureToggleRepository;

    @InjectMocks
    private FeatureToggleServiceImpl underTest;

    @Test
    public void createFeatureToggleTest() {
        FeatureToggleDTO featureToggleDTO = FeatureToggleDTO.builder()
                .technicalName("feature-1")
                .inverted(false)
                .customerIds(List.of("1", "2"))
                .build();

        List<Customer> customerList = List.of(Customer.builder().id(1L).name("Customer1").build(),
                Customer.builder().id(2L).name("Customer2").build());

        Mockito.when(customerRepository.findAllById(List.of(1L, 2L)))
                .thenReturn(customerList);

        Mockito.when(featureToggleRepository.save(Mockito.any(FeatureToggle.class)))
                .thenReturn(FeatureToggle.builder()
                .technicalName("feature-1")
                .inverted(false)
                .archived(false)
                .featureToggleCustomers(customerList)
                .build());

        FeatureToggleDTO testResult = underTest.createFeatureToggle(featureToggleDTO);

        Assertions.assertNotNull(testResult);
        Assertions.assertEquals("feature-1", testResult.getTechnicalName());
        Assertions.assertEquals(false, testResult.getInverted());
        Assertions.assertEquals(2, testResult.getCustomerIds().size());
    }

    @Test
    public void updateFeatureToggleTest() {
        FeatureToggleDTO featureToggleDTO = FeatureToggleDTO.builder()
                .technicalName("feature-1")
                .inverted(false)
                .customerIds(List.of("1", "2"))
                .build();

        List<Customer> customerList = List.of(Customer.builder().id(1L).name("Customer1").build(),
                Customer.builder().id(2L).name("Customer2").build());

        Mockito.when(featureToggleRepository.findFeatureToggleByTechnicalName("feature-1"))
                .thenReturn(FeatureToggle.builder()
                        .technicalName("feature-1")
                        .inverted(false)
                        .archived(false)
                        .featureToggleCustomers(customerList)
                        .build());

        Mockito.when(customerRepository.findAllById(List.of(1L, 2L)))
                .thenReturn(customerList);

        Mockito.when(featureToggleRepository.save(Mockito.any(FeatureToggle.class)))
                .thenReturn(FeatureToggle.builder()
                        .technicalName("feature-1")
                        .inverted(true)
                        .archived(false)
                        .featureToggleCustomers(customerList)
                        .build());

        FeatureToggleDTO testResult = underTest.updateFeatureToggle(featureToggleDTO);

        Assertions.assertNotNull(testResult);
        Assertions.assertEquals("feature-1", testResult.getTechnicalName());
        Assertions.assertEquals(true, testResult.getInverted());
        Assertions.assertEquals(2, testResult.getCustomerIds().size());
    }

    @Test
    public void archiveFeatureToggleTest() {
        List<Customer> customerList = List.of(Customer.builder().id(1L).name("Customer1").build(),
                Customer.builder().id(2L).name("Customer2").build());

        Mockito.when(featureToggleRepository.findFeatureToggleByTechnicalName("feature-1"))
                .thenReturn(FeatureToggle.builder()
                        .technicalName("feature-1")
                        .inverted(false)
                        .archived(false)
                        .featureToggleCustomers(customerList)
                        .build());

        Mockito.when(featureToggleRepository.save(Mockito.any(FeatureToggle.class)))
                .thenReturn(FeatureToggle.builder()
                        .technicalName("feature-1")
                        .inverted(false)
                        .archived(true)
                        .featureToggleCustomers(customerList)
                        .build());

        FeatureToggleDTO testResult = underTest.archiveFeatureToggle("feature-1");

        Assertions.assertNotNull(testResult);
        Assertions.assertEquals("feature-1", testResult.getTechnicalName());
        Assertions.assertEquals(false, testResult.getInverted());
        Assertions.assertEquals(2, testResult.getCustomerIds().size());
    }

    @Test
    public void getAllFeatureTogglesTest() {
        List<Customer> customerList = List.of(Customer.builder().id(1L).name("Customer1").build(),
                Customer.builder().id(2L).name("Customer2").build());

        List<FeatureToggle> featureToggleList = new ArrayList<>();
        featureToggleList.add(FeatureToggle.builder()
        .technicalName("feature-1")
        .inverted(false)
        .archived(false)
        .featureToggleCustomers(customerList)
        .build());

        featureToggleList.add(FeatureToggle.builder()
                .technicalName("feature-2")
                .inverted(true)
                .archived(false)
                .featureToggleCustomers(Collections.emptyList())
                .build());

        Mockito.when(featureToggleRepository.getAllByArchivedFalse()).thenReturn(featureToggleList);

        List<FeatureToggleDTO> testResult = underTest.getAllFeatureToggles();

        Assertions.assertNotNull(testResult);
        Assertions.assertEquals(2, testResult.size());
        Assertions.assertEquals(2, testResult.get(0).getCustomerIds().size());
        Assertions.assertEquals(0, testResult.get(1).getCustomerIds().size());
        Assertions.assertEquals("feature-1", testResult.get(0).getTechnicalName());
        Assertions.assertEquals("feature-2", testResult.get(1).getTechnicalName());
    }

    @Test
    public void compareFeaturesByCustomerTest() {

        Customer customer = Customer.builder().id(1L).name("Customer-1").build();

        Mockito.when(customerRepository.findById(1L))
                .thenReturn(Optional.of(customer));

        Mockito.when(featureToggleRepository.findFeatureToggleByTechnicalName("feature-1"))
                .thenReturn(FeatureToggle.builder()
                .technicalName("feature-1")
                .inverted(false)
                .archived(false)
                .featureToggleCustomers(List.of(customer))
                .build());

        Mockito.when(featureToggleRepository.findFeatureToggleByTechnicalName("feature-2"))
                .thenReturn(FeatureToggle.builder()
                        .technicalName("feature-2")
                        .inverted(false)
                        .archived(false)
                        .featureToggleCustomers(Collections.emptyList())
                        .build());

        FeatureResponseWrapperDTO testResult = underTest.compareFeaturesByCustomer(FeatureRequestDTO.builder()
                .customerId("1")
                .features(List.of(FeatureDTO.builder().name("feature-1").build(), FeatureDTO.builder().name("feature-2").build()))
                .build());

        Assertions.assertNotNull(testResult);
        Assertions.assertEquals(2, testResult.getFeatures().size());
        Assertions.assertEquals("feature-1", testResult.getFeatures().get(0).getName());
        Assertions.assertEquals(true, testResult.getFeatures().get(0).getActive());
        Assertions.assertEquals("feature-2", testResult.getFeatures().get(1).getName());
        Assertions.assertEquals(false, testResult.getFeatures().get(1).getActive());
    }

}
