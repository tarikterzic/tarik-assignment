package ch.swisscom.management.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class FeatureResponseDTO {

    private String name;
    private Boolean active;
    private Boolean inverted;
    private Boolean expired;
}
