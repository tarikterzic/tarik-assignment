package ch.swisscom.management.controller;

import ch.swisscom.management.dto.FeatureToggleDTO;
import ch.swisscom.management.service.IFeatureToggleService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@AllArgsConstructor
@RestController
public class FeatureToggleController {

    private final IFeatureToggleService featureToggleService;

    @PostMapping(value = "featureToggles")
    public ResponseEntity<FeatureToggleDTO> createFeatureToggle(@RequestBody @Valid FeatureToggleDTO featureToggleDTO) {
        return ResponseEntity.ok(featureToggleService.createFeatureToggle(featureToggleDTO));
    }

    @PutMapping(value = "featureToggles")
    public ResponseEntity<FeatureToggleDTO> updateFeatureToggle(@RequestBody @Valid FeatureToggleDTO featureToggleDTO) {
        return ResponseEntity.ok(featureToggleService.updateFeatureToggle(featureToggleDTO));
    }

    @PutMapping(value = "featureToggles/{technicalName}")
    public ResponseEntity<FeatureToggleDTO> archiveFeatureToggle(@PathVariable @NotNull String technicalName) {
        return ResponseEntity.ok(featureToggleService.archiveFeatureToggle(technicalName));
    }

    @GetMapping(value = "featureToggles")
    public ResponseEntity<List<FeatureToggleDTO>> getAllFeatureToggles() {
        return ResponseEntity.ok(featureToggleService.getAllFeatureToggles());
    }
}
