package ch.swisscom.management.service;

import ch.swisscom.management.dto.*;
import ch.swisscom.management.persistence.entity.Customer;
import ch.swisscom.management.persistence.entity.FeatureToggle;
import ch.swisscom.management.persistence.repository.CustomerRepository;
import ch.swisscom.management.persistence.repository.FeatureToggleRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class FeatureToggleServiceImpl implements IFeatureToggleService {

    private final FeatureToggleRepository featureToggleRepository;
    private final CustomerRepository customerRepository;

    @Override
    public FeatureToggleDTO createFeatureToggle(FeatureToggleDTO featureToggleDTO) {

        FeatureToggle featureToggleEntity = new FeatureToggle();

        convertToEntity(featureToggleEntity, featureToggleDTO);
        featureToggleEntity.setArchived(false);

        featureToggleEntity = featureToggleRepository.save(featureToggleEntity);

        return convertToDTO(featureToggleEntity);
    }

    @Override
    public FeatureToggleDTO updateFeatureToggle(FeatureToggleDTO featureToggleDTO) {
        FeatureToggle featureToggleEntity = featureToggleRepository
                .findFeatureToggleByTechnicalName(featureToggleDTO.getTechnicalName());

        if (featureToggleEntity == null) {
            throw new EntityNotFoundException("Feature toggle with provided technical name not found");
        }

        if (featureToggleEntity.getArchived()) {
            throw new EntityNotFoundException("Feature toggle with provided technical name is archived");
        }

        convertToEntity(featureToggleEntity, featureToggleDTO);

        featureToggleEntity = featureToggleRepository.save(featureToggleEntity);

        return convertToDTO(featureToggleEntity);

    }

    @Override
    public FeatureToggleDTO archiveFeatureToggle(String technicalName) {

        FeatureToggle featureToggleEntity = featureToggleRepository.findFeatureToggleByTechnicalName(technicalName);

        if (featureToggleEntity == null) {
            throw new EntityNotFoundException("Feature toggle with provided technical name not found");
        }

        featureToggleEntity.setArchived(true);
        featureToggleEntity = featureToggleRepository.save(featureToggleEntity);

        return convertToDTO(featureToggleEntity);

    }

    @Override
    public List<FeatureToggleDTO> getAllFeatureToggles() {
        List<FeatureToggle> featureToggleList = featureToggleRepository.getAllByArchivedFalse();

        return featureToggleList.stream().map(this::convertToDTO).collect(Collectors.toList());
    }

    @Override
    public FeatureResponseWrapperDTO compareFeaturesByCustomer(FeatureRequestDTO featureRequestDTO) {

        List<FeatureResponseDTO> featureResponseDTOList = new ArrayList<>();

        Customer customer = customerRepository.findById(Long.parseLong(featureRequestDTO.getCustomerId()))
                .orElse(null);

        if (customer == null) {
            return FeatureResponseWrapperDTO.builder()
                    .features(featureResponseDTOList)
                    .build();
        }

        for (FeatureDTO featureDTO : featureRequestDTO.getFeatures()) {

            FeatureToggle featureToggleEntity = featureToggleRepository
                    .findFeatureToggleByTechnicalName(featureDTO.getName());

            // if feature is existing and not archived
            if (featureToggleEntity != null && !featureToggleEntity.getArchived()) {

                boolean expired = featureToggleEntity.getExpiresOn() != null &&
                        featureToggleEntity.getExpiresOn().before(Timestamp.valueOf(LocalDateTime.now()));

                featureResponseDTOList.add(FeatureResponseDTO.builder()
                        .name(featureDTO.getName())
                        .active(featureToggleEntity.getFeatureToggleCustomers().contains(customer))
                        .inverted(featureToggleEntity.getInverted())
                        .expired(expired)
                        .build());
            }
        }

        return FeatureResponseWrapperDTO.builder()
                .features(featureResponseDTOList)
                .build();
    }

    private FeatureToggleDTO convertToDTO(FeatureToggle entity) {

        List<String> customerIds = new ArrayList<>();

        for (Customer customer : entity.getFeatureToggleCustomers()) {
            customerIds.add(customer.getId().toString());
        }

        return FeatureToggleDTO.builder()
                .displayName(entity.getDisplayName())
                .technicalName(entity.getTechnicalName())
                .expiresOn(entity.getExpiresOn() == null ? null : entity.getExpiresOn().toLocalDateTime())
                .description(entity.getDescription())
                .inverted(entity.getInverted())
                .customerIds(customerIds)
                .build();
    }

    private void convertToEntity(FeatureToggle featureToggleEntity, FeatureToggleDTO featureToggleDTO) {

        featureToggleEntity.setDisplayName(featureToggleDTO.getDisplayName());
        featureToggleEntity.setTechnicalName(featureToggleDTO.getTechnicalName());
        featureToggleEntity.setExpiresOn(featureToggleDTO.getExpiresOn() == null ?
                null : Timestamp.valueOf(featureToggleDTO.getExpiresOn()));
        featureToggleEntity.setDescription(featureToggleDTO.getDescription());
        featureToggleEntity.setInverted(featureToggleDTO.getInverted());

        List<Customer> customerList = customerRepository
                .findAllById(featureToggleDTO.getCustomerIds().stream().map(Long::parseLong)
                        .collect(Collectors.toSet()));

        featureToggleEntity.setFeatureToggleCustomers(new HashSet<>(customerList));
    }
}
