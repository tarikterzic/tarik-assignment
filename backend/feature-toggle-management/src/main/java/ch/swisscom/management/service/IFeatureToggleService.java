package ch.swisscom.management.service;

import ch.swisscom.management.dto.FeatureRequestDTO;
import ch.swisscom.management.dto.FeatureResponseWrapperDTO;
import ch.swisscom.management.dto.FeatureToggleDTO;

import java.util.List;

public interface IFeatureToggleService {

    FeatureToggleDTO createFeatureToggle(FeatureToggleDTO featureToggleDTO);

    FeatureToggleDTO updateFeatureToggle(FeatureToggleDTO featureToggleDTO);

    FeatureToggleDTO archiveFeatureToggle(String technicalName);

    List<FeatureToggleDTO> getAllFeatureToggles();

    FeatureResponseWrapperDTO compareFeaturesByCustomer(FeatureRequestDTO featureRequestDTO);
}
