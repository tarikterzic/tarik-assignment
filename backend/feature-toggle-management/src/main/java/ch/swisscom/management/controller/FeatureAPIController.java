package ch.swisscom.management.controller;

import ch.swisscom.management.dto.FeatureRequestDTO;
import ch.swisscom.management.dto.FeatureResponseWrapperDTO;
import ch.swisscom.management.service.IFeatureToggleService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@AllArgsConstructor
@RestController
public class FeatureAPIController {

    private final IFeatureToggleService featureToggleService;

    @PostMapping(value = "api/v1/features")
    public ResponseEntity<FeatureResponseWrapperDTO> compareFeaturesByCustomer(@RequestBody @Valid FeatureRequestDTO featureRequestDTO) {
        return ResponseEntity.ok(featureToggleService.compareFeaturesByCustomer(featureRequestDTO));
    }
}
