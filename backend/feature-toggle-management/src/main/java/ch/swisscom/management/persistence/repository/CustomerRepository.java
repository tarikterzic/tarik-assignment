package ch.swisscom.management.persistence.repository;

import ch.swisscom.management.persistence.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

}
