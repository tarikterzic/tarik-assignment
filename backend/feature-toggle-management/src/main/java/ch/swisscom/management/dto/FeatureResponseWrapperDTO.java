package ch.swisscom.management.dto;

import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class FeatureResponseWrapperDTO {
    private List<FeatureResponseDTO> features;
}
