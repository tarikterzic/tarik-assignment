package ch.swisscom.management.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@Builder
@Getter
@Setter
public class FeatureToggleDTO {

    private String displayName;
    @NotNull
    private String technicalName;
    private LocalDateTime expiresOn;
    private String description;
    @NotNull
    private Boolean inverted;
    @NotNull
    private List<String> customerIds;

}
