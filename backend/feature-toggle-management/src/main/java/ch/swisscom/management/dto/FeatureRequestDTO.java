package ch.swisscom.management.dto;

import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class FeatureRequestDTO {

    private String customerId;
    private List<FeatureDTO> features;
}
