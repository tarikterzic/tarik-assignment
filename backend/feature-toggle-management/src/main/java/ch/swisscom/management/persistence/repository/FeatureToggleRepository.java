package ch.swisscom.management.persistence.repository;


import ch.swisscom.management.persistence.entity.FeatureToggle;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FeatureToggleRepository extends JpaRepository<FeatureToggle, Long> {

    FeatureToggle findFeatureToggleByTechnicalName(String technicalName);

    List<FeatureToggle> getAllByArchivedFalse();
}
