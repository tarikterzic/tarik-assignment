package ch.swisscom.management.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class FeatureDTO {
    private String name;
}
