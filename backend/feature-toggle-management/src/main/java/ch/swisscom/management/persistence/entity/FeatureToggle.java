package ch.swisscom.management.persistence.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name = "feature_toggle")
public class FeatureToggle {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    @Column(name = "display_name")
    private String displayName;

    @Column(name = "technical_name", nullable = false)
    private String technicalName;

    @Column(name = "expires_on")
    private Timestamp expiresOn;

    @Column(name = "description")
    private String description;

    @Column(name = "inverted", nullable = false)
    private Boolean inverted;

    @Column(name = "archived")
    private Boolean archived;

    @Singular
    @ManyToMany
    @JoinTable(name = "feature_toggle_customer",
            joinColumns = {@JoinColumn(name = "feature_toggle_id",
            referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "FK_feature_toggle_customer_param_feature_toggle_id"))},
            inverseJoinColumns = {@JoinColumn(name = "customer_id",
            referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "FK_feature_toggle_customer_param_customer_id"))}
    )
    private Set<Customer> featureToggleCustomers;
}
